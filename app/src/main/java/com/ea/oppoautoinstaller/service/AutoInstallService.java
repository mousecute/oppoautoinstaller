package com.ea.oppoautoinstaller.service;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.ClipData;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.content.ClipboardManager;

import com.goebl.david.Webb;

import org.json.JSONObject;

import java.io.DataOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import static android.view.accessibility.AccessibilityEvent.TYPES_ALL_MASK;

public class AutoInstallService extends AccessibilityService {
    private String _kraken_server_host = "localhost";
    private String _kraken_server_port = "80";
    private String _adb_devicd_id;
    private String _device_password = "123456";

    private PreferenceListener _prefListener = new PreferenceListener();

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        Log.d("AutoInstallService", "onAccessibilityEvent");
        Log.d("AutoInstallService", new StringBuilder()
                .append(event.getPackageName().toString()).append("\n")
                .append(event.getEventType()).append("\n")
                .append(event.toString())
                .toString()
        );
        //针对oppo r9s，oppo手机上就是很麻烦需要点击多步安装
//        clickInstallButton("com.android.packageinstaller:id/btn_allow_once");//点击安装按钮
//        clickInstallButton("com.android.packageinstaller:id/ok_button");//点击安装按钮
//        clickInstallButton("com.android.packageinstaller:id/bottom_button_one");//点击完成按钮
        if (event.getPackageName().equals("com.android.packageinstaller") || event.getPackageName().equals("com.coloros.safecenter"))
        {
            sendInstallPassword("com.coloros.safecenter:id/et_login_passwd_edit", _device_password);//输入安装密码123456,不同的oppo时id还不一样
            clickInstallButton("android:id/button1");//点击安装按钮
            clickInstallButtonText("安装", true); // com.android.packageinstaller
        }
    }

    private void _loadPreference()
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        _device_password = prefs.getString("password_text", _device_password);
        _kraken_server_host = prefs.getString("kraken_host_text", _kraken_server_host);
        _kraken_server_port = prefs.getString("kraken_port_text", _kraken_server_port);
        Log.d("AutoInstallService", "_loadPreference: password = " + _device_password + ", kraken server host = " + _kraken_server_host + ", kraken server port = " + _kraken_server_port);
    }

    private void _registerPreference()
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.registerOnSharedPreferenceChangeListener(_prefListener);
    }

    private void _unregisterPreference()
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.unregisterOnSharedPreferenceChangeListener(_prefListener);
    }

    private class PreferenceListener implements SharedPreferences.OnSharedPreferenceChangeListener
    {
        @Override
        // TODO: Listener is never called? Service must be restarted after preference is chaged!
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
        {
            Log.d("AutoInstallService", "onSharedPreferenceChaned: key = " + key + ", value = " + sharedPreferences.getString(key, null));
            if (key == "password_text"){
                _device_password = sharedPreferences.getString(key, _device_password);
            } else if (key == "kraken_host_text"){
                _kraken_server_host = sharedPreferences.getString(key, _kraken_server_host);
            } else if (key == "kraken_port_text"){
                _kraken_server_port = sharedPreferences.getString(key, _kraken_server_port);
            }
        }
    }

    private void sendInstallPassword(String id,String text){
        if(getRootInActiveWindow() == null) {
            Log.d("AutoInstallService", "this.getRootInActiveWindow() == null");
            return;
        }
        try{
            List<AccessibilityNodeInfo> nodes = getRootInActiveWindow().findAccessibilityNodeInfosByViewId(id);
            Log.d("AutoInstallService", "nodes.size() is " + nodes.size());
            if(nodes.size()==1){
                Log.d("AutoInstallService", "m的值："+text);
                AccessibilityNodeInfo node = nodes.get(0);
                AccessibilityNodeInfo target = node.findFocus(AccessibilityNodeInfo.FOCUS_INPUT);
                if (target == null) {
                    Log.d("AutoInstallService", "inputPassword: null");
                    return;
                }
                ClipboardManager clipboard  = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label",text);
                clipboard.setPrimaryClip(clip);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                    target.performAction(AccessibilityNodeInfo.ACTION_PASTE);
                }
            }
        }catch (NullPointerException e){
            Log.d("AutoInstallService","没有找到控件");
        }
    }

    private void clickInstallButton(String id){
        if(getRootInActiveWindow() == null){
            return;
        }
        try{
            //以下操作针对oppo手机上，点击'继续安装'按钮
            List<AccessibilityNodeInfo> nodes = getRootInActiveWindow().findAccessibilityNodeInfosByViewId(id);
            for(int i = 0;i<nodes.size();i++){
                AccessibilityNodeInfo node = nodes.get(i);
                node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
            }
        }catch (NullPointerException e){
            Log.d("AutoInstallService","没有找到控件");
        }
    }

    private void clickInstallButtonText(String text, Boolean bUseAdbShellInput){
        if (getRootInActiveWindow() == null) {
            return;
        }
        try {
            Log.d("AutoInstallService", "try to find button with text == " + text);
            List<AccessibilityNodeInfo> nodes = getRootInActiveWindow().findAccessibilityNodeInfosByText(text);
            for (int i = 0; i < nodes.size(); i++) {
                AccessibilityNodeInfo node = nodes.get(i);
                Log.d("AutoInstallService", "    button text: " + node.getText());
                if(node.getText().equals(text)){
                    Log.d("AutoInstallService", "    click button viewIdResourceName: " + node.getViewIdResourceName());
                    Log.d("AutoInstallService", "    click button windowId: " + node.getWindowId());
                    Log.d("AutoInstallService", "    click button info: \n" + node.toString());

                    Class c = Class.forName("android.view.accessibility.AccessibilityNodeInfo");
                    Method m = c.getMethod("getSourceNodeId", new Class[]{});
                    Object o = m.invoke(node, new Object[]{});
                    long sourceNodeId = (long)o;
                    Log.d("AutoInstallService", "source node id: " + o.toString());
                    m = c.getMethod("getConnectionId", new Class[]{});
                    o = m.invoke(node, new Object[]{});
                    int connectionId = (int)o;
                    Log.d("AutoInstallService", "connection id:" + o.toString());

                    if (bUseAdbShellInput == false)
                    {
                        Boolean actionResult = node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        Log.d("AutoInstallService", "    action result: " + actionResult.toString());
                    }
                    else
                    {
                        Rect r = new Rect();
                        node.getBoundsInScreen(r);
                        sendTouchEvent((r.left + r.right) / 2, (r.bottom + r.top) / 2);
                    }
                }
            }
        } catch (NullPointerException e){
            Log.d("AutoInstallService", "没有找到控件");
        } catch (ClassNotFoundException e){
            Log.d("AutoInstallService", e.toString());
        } catch (NoSuchMethodException e){
            Log.d("AutoInstallService", e.toString());
        } catch (InvocationTargetException e){
            Log.d("AutoInstallService", e.toString());
        } catch (IllegalAccessException e){
            Log.d("AutoInstallService", e.toString());
        }
    }

    private void sendTouchEvent(int x, int y){
        try{
            final String touchCmd = new StringBuilder().append("input tap ").append(x).append(" ").append(y).toString();
            // _executeCmd(touchCmd);
            final String touchUrl = new StringBuilder().append("http://").append(_kraken_server_host).append(":").append(_kraken_server_port).append("/api/device/").append(_adb_devicd_id).append("/shell/run").toString();
            Log.d("AutoInstallService", "Send command to kraken: " + touchUrl);

            // _sendViaURLConnection(touchCmd, touchUrl);
            _sendViaDavidWebb(touchCmd, touchUrl);
        }catch (Exception e) {
            Log.d("AutoInstallService", e.toString());
        }
    }

    private void _executeCmd(final String cmd)
    {
        try {
            Process process = Runtime.getRuntime().exec(cmd);
            Log.d("AutoInstallService", cmd + " result: " + process.waitFor());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void _sendViaURLConnection(final String cmd, final String url)
    {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL oUrl = new URL(url);
                    HttpURLConnection conn = (HttpURLConnection) oUrl.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setDoOutput(true);
                    conn.setDoInput(true);

                    JSONObject jsonParam = new JSONObject();
                    jsonParam.put("cmd", cmd);

                    Log.i("JSON", jsonParam.toString());
                    DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                    os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
//                    os.writeBytes(jsonParam.toString());

                    os.flush();
                    os.close();

                    conn.getInputStream();

                    Log.i("STATUS", String.valueOf(conn.getResponseCode()));
                    Log.i("MSG" , conn.getResponseMessage());

                    conn.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
    }

    private void _sendViaDavidWebb(final String cmd, final String url)
    {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Webb webb = Webb.create();
                    JSONObject result = webb
                            .get(url)
                            .param("cmd", cmd)
                            .ensureSuccess()
                            .asJsonObject()
                            .getBody();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
    }

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        Log.d("AutoInstallService","onServiceConnected");

        _adb_devicd_id = Build.SERIAL;
        Log.i("AutoInstallService", "adb device id: " + _adb_devicd_id);

        _loadPreference();
        _registerPreference();

        AccessibilityServiceInfo info = this.getServiceInfo();
        Log.d("AutoInstallService", new StringBuilder()
                .append(" info.eventTypes ").append(info.eventTypes)
                .append(" info.feedbackType ").append(info.feedbackType)
                .append(" info.flags ").append(info.flags)
                .append(" info.packageNames ").append(info.packageNames)
                .append(" can retrieve windows content ").append(info.getCapabilities() & AccessibilityServiceInfo.CAPABILITY_CAN_RETRIEVE_WINDOW_CONTENT)
        .toString());

//        info.eventTypes = AccessibilityEvent.TYPES_ALL_MASK;
//        info.flags = AccessibilityServiceInfo.DEFAULT;
//        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_ALL_MASK;
//        this.setServiceInfo(info);
//
//        info = this.getServiceInfo();
//        Log.d("AutoInstallService", new StringBuilder()
//                .append(" info.eventTypes ").append(info.eventTypes)
//                .append(" info.feedbackType ").append(info.feedbackType)
//                .append(" info.flags ").append(info.flags)
//                .append(" info.packageNames ").append(info.packageNames)
//                .append(" can retrieve windows content ").append(info.getCapabilities() | AccessibilityServiceInfo.CAPABILITY_CAN_RETRIEVE_WINDOW_CONTENT)
//                .toString());
    }

    @Override
    public void onInterrupt() {
        Log.d("AutoInstallService","onInterrupt");
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d("AutoInstallService", "onUnBind");
        _unregisterPreference();

        return super.onUnbind(intent);
    }
}
